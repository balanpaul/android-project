package com.balanpaul.dishfire;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.balanpaul.dishfire.R;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Balan Paul on 1/15/2019.
 */

public class SplashActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                Intent i = new Intent(SplashActivity.this, Login.class);
                startActivity(i);
                finish();
            }
        }.start();
    }
}
