package com.balanpaul.dishfire.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balanpaul.dishfire.R;
import com.balanpaul.dishfire.vo.Dish;

import java.util.List;


/**
 * Created by Balan Paul on 7/31/2018.
 * adapter
 */

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.MyViewHolder> {

    private Context mContext;
    private List<Dish> dishList;
    private Listener mListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, price,descrption,venue;
        ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            descrption = view.findViewById(R.id.desription);
            venue = view.findViewById(R.id.venue);
            overflow = view.findViewById(R.id.overflow);
//            thumbnail = view.findViewById(R.id.thumbnail);
        }
    }


    public DishAdapter(Context mContext, List<Dish> albumList, Listener listener) {
        this.mContext = mContext;
        this.dishList = albumList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dish_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Dish dish = dishList.get(position);

                holder.title.setText(dish.getName());
                holder.price.setText(dish.getPrice());
                holder.descrption.setText(dish.getDescription());
                holder.venue.setText(dish.getVenue());
                holder.overflow.setOnClickListener(v -> showPopupMenu(holder.overflow, position));

    }


    /**
     * Showing popup menu when tapping on 3 dots
     */
    private int showPopupMenu(View view, int pos) {
        // inflate menu

        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(pos));
        popup.show();
        return pos;
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        int position;

        MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_remove:
                    //Toast.makeText(mContext, (int) getItemId(1), Toast.LENGTH_SHORT).show();
                    mListener.onSelectItem(position, "action_remove");
                    return true;
                case R.id.action_search:
                    mListener.onSelectItem(position, "action_search");
                    return true;
                case R.id.action_edit:
                    mListener.onSelectItem(position, "action_edit");
                    return true;
                default:
            }
            return false;

        }

    }
    @Override
    public int getItemCount() {
        return dishList.size();
    }

    public interface Listener {
        void onSelectItem(int position, String action);
    }
}
