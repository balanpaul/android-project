package com.balanpaul.dishfire.vo;

/**
 * Created by Balan Paul on 12/30/2018.
 */

public class Dish {
    private String id;
    private String name;
    private String price;
    private String description;
    private String venue;

    public Dish() {
    }

    public Dish(String id, String name, String price, String description, String venue) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.venue = venue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }
}
