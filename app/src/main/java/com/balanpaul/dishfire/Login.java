package com.balanpaul.dishfire;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;




public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    //    private TextView statusTextView;
//    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;
    private EditText username;
    private EditText password;
    private DatabaseReference databaseReference;
    FirebaseUser user;
    boolean doubleBackToExitPressedOnce = false;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        View view = findViewById(R.id.login_box);

        doubleBackToExitPressedOnce = false;
        View view1 = view.findViewById(R.id.linear_layout_box);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        Button signIn = view1.findViewById(R.id.signInButton);
        username = view1.findViewById(R.id.username);
        password = view1.findViewById(R.id.password);
        Button registerButton = view1.findViewById(R.id.register);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        signIn.setOnClickListener(this);
        password.setOnClickListener(this);
        registerButton.setOnClickListener(this);
       FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
       updateUI(firebaseUser);

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case (R.id.register):
                register();
                break;


            case (R.id.signInButton):
                if (username.getText().length() > 0 && password.getText().length() > 0)
                    signInMail(username.getText().toString(), password.getText().toString());
                else

                    Toast.makeText(getApplicationContext(), "Introdu toate campurile", Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "apasa din nou pt a iesi", Toast.LENGTH_SHORT).show();
    }

    private void register() {
        Intent registerIntent = new Intent(Login.this, Register.class);
        startActivity(registerIntent);
        finish();

    }




    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }




    private void signInMail(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (isValidEmail(email) && password.length() > 5) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (user != null)
                                    updateUI(user);


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            if (task.getException() != null)
                                Toast.makeText(Login.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]

                    });
        } else {
            Toast.makeText(Login.this,"Date invalide",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValidEmail(String target) {
        return true;
    }

    private void updateUI(final FirebaseUser user) {
        boolean verif;

        if (user != null ) {

            Intent registerIntent = new Intent(Login.this, MainActivity.class);
            startActivity(registerIntent);
        } else {
                Toast.makeText(Login.this, "Nu esti autentificat",
                        Toast.LENGTH_SHORT).show();
        }
    }


}

