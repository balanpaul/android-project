package com.balanpaul.dishfire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by Alex on 29.04.2018.
 */


public class Register extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SignInActivity";
    private Button btnSubmit;
    private EditText editTextPassword;
    private EditText doubleCheckPassword;
    private EditText editTextMail;
    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;


    public void onCreate(Bundle savedInstanceState) {
        // getActionBar().setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();
        editTextPassword = findViewById(R.id.password);

        doubleCheckPassword = findViewById(R.id.doublecheck);
        editTextMail = findViewById(R.id.email);
        btnSubmit = findViewById(R.id.register);
        btnSubmit.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        final String email = editTextMail.getText().toString();
        final String password = editTextPassword.getText().toString();
        final String doublepassword = doubleCheckPassword.getText().toString();
        switch (view.getId()) {

            case (R.id.register):
                if(isEmailValid(email))
                createAccount();
                break;
        }
    }

    @Override
    public void onBackPressed() {

            Intent i = new Intent(Register.this, Login.class);
            startActivity(i);
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }




    private void createAccount() {
        try {
            String email = editTextMail.getText().toString();
            final String password = editTextPassword.getText().toString();
            final String checkPassword = doubleCheckPassword.getText().toString();

            Log.d(TAG, "createAccount:" + email);
            // [START create_user_with_email]
            if (comparePassword(password, checkPassword)) {


                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
//
                                Toast.makeText(Register.this, "Account created.", Toast.LENGTH_SHORT).show();


                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(Register.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();

                            }

                        });

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Register/createAccount", e.getMessage());
        }


    }

    public boolean comparePassword(String password, String doubleCheckPassword) {

        return password.equals(doubleCheckPassword);
    }

}
