package com.balanpaul.dishfire;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.balanpaul.dishfire.ui.DishFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Intent intent=new Intent(this,Login.class);
//        startActivity(intent);
        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, DishFragment.newInstance())
                    .commitNow();
        }
    }
}
